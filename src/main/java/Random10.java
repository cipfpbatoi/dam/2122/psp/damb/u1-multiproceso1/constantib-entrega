import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Random10 {
    public static void main(String[] args) throws IOException {
        Random random = new Random();
        int num_random;
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));

        String frases;
        boolean bucle = true;
        while (bucle && (frases = bfr.readLine()) != null) {
            if (frases.equalsIgnoreCase("stop")) {
                bucle = false;
            } else {
                num_random = random.nextInt(11);
                System.out.println(num_random);
            }
        }
    }
}
